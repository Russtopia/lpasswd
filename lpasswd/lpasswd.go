package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"blitter.com/go/lpasswd/internal/termmode"
	"blitter.com/go/lpasswd"
)

var (
	version   string = "0.1"
	gitCommit string
)

// nolint: gocyclo
func main() {
	var vopt bool
	var pfName string
	var newpw string
	var confirmpw string
	var userName string

	flag.BoolVar(&vopt, "v", false, "show version")
	flag.StringVar(&userName, "u", "", "username")
	flag.StringVar(&pfName, "f", "local.passwd", "passwd file")
	flag.Parse()

	if vopt {
		if gitCommit != "" {
			gitCommit = fmt.Sprintf(" (%s)", gitCommit)
		}
		fmt.Printf("version %s%s\n", version, gitCommit)
		os.Exit(0)
	}

	var uname string
	if len(userName) == 0 {
		log.Println("specify username with -u")
		os.Exit(1)
	}

	//u, err := user.Lookup(userName)
	//if err != nil {
	//	log.Printf("Invalid user %s\n", userName)
	//	log.Fatal(err)
	//}
	//uname = u.Username
	uname = userName

	fmt.Printf("New Password:")
	ab, err := termmode.ReadPassword(os.Stdin.Fd())
	fmt.Printf("\r\n")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	newpw = string(ab)

	fmt.Printf("Confirm:")
	ab, err = termmode.ReadPassword(os.Stdin.Fd())
	fmt.Printf("\r\n")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	confirmpw = string(ab)

	if confirmpw != newpw {
		log.Println("New passwords do not match.")
		os.Exit(1)
	}
	
	err = lpasswd.SetPasswd(uname, newpw, pfName)
	if err != nil {
		log.Fatal(err)
	}
}
