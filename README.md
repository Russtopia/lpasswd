Simple library to manage and verify logins against app-local or system accounts. Includes a simple CLI util to add/set user accounts to a local password file using bcrypt.

Typical uses include CLI login or HTTP basic authentication.